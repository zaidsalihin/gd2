//scr_attack_state

// Get Move Axis
var xdir=(key_right - key_left);

image_speed = 0.5;

if (xdir != 0) image_xscale = xdir;
sprite_index=spr_player_idle;

//At attack frame >=3 and if attacked=false, check if attack hits anything,
//then apply impulse to attacked actor.

if (image_index >=0 && attacked= false)
{
    var xx = 0;
    var yy = 0;

    xx = x+(8*image_xscale);
    yy = y+2;

    var damage = instance_create(xx,yy,obj_damage);
    damage.creator=id;
    attacked=true;
}
