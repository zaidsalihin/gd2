///scr_get_input
/////////////////////////////////////////////////////////////////////////////

key_right = keyboard_check(vk_right);
key_left = keyboard_check(vk_left);
key_down = keyboard_check(vk_down);
key_up = keyboard_check_pressed(vk_up);

//key_jump = keyboard_check_pressed(vk_up);
key_jump = keyboard_check_pressed(vk_space);
key_jump_released = keyboard_check_released(vk_space);

key_attack = keyboard_check_pressed(ord('X'));
key_freeze = keyboard_check_pressed(ord('F'));
key_pull = keyboard_check(vk_control);

key_enter = keyboard_check_pressed(vk_enter);
key_restart = keyboard_check_pressed(ord('R'));
key_paused = keyboard_check_pressed(vk_escape);
key_quit = keyboard_check_pressed(ord('Q'));
