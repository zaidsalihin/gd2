///scr_push(object)
var object = argument[0];

// Horizontal pull/push

/*  We check for collision that occurs behind player facing direction
    using (2*-(h_speed)), where 2 is a buffer multiplier and -(h_speed)
    being the space behind player facing direction */
     
if(key_pull && place_meeting(x+(5*-(h_speed)), y, object)){
    show_debug_message('pullinh');
    var object_collided = instance_place(x+(5*-(h_speed)), y, object);
    with(object_collided){
            scr_move(obj_player.h_speed/2, 0);
            image_speed = 0.25;
        }
    h_speed /= 2;
} else if(place_meeting(x+h_speed/2, y, object)){
    var object_collided = instance_place(x+h_speed/2, y, object);
    with(object_collided){
        scr_move(obj_player.h_speed/2, 0);
        image_speed = 0.25;
    }
    h_speed /= 2;
}

// Vertical push

if(place_meeting(x, y+vspeed/2, object)){
    var object_collided = instance_place(x, y+vspeed/2, object);
    with(object_collided){
        scr_move(obj_player.v_speed/2, 0);
    }
    v_speed /= 2;
}


