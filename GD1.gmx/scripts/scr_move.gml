///scr_move(h_speed, v_speed)
var h_speed = argument[0];
var v_speed = argument[1];

// Horizontal Collision

if(place_meeting(x+h_speed, y, obj_solid)){
    while(!place_meeting(x+sign(h_speed), y, obj_solid)){
        x += sign(h_speed);
    }
    h_speed = 0;
} 

// Move Horizontally (x has priority over y, to avoid buggy movement)

x += h_speed;

if (h_speed == 0  ){
    //sprite_index = spr_player_idle;
    image_index = 0;
}
 
// Vertical Collision

if(place_meeting(x, y+v_speed, obj_solid)){
    while(!place_meeting(x, y+sign(v_speed), obj_solid)){
        y += sign(v_speed);
         }
    v_speed = 0;
} 

// Move Vertically

y += v_speed;

