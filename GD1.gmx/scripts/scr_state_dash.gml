///scr_state_dash
/////////////////////////////////////////////////////////////////////////////

event_inherited();
// Set Dash speed
var dashspeed = movespeed * 0.5;

// Set hspd and vspd
//hspd = dashspeed * xdir;
//physics_apply_local_impulse(0,0,xdir * 1.25  ,ydir*-1.1);
physics_apply_local_impulse(0,0,xdir * 1.25  ,0);
// Move character
phy_position_x += hspd;
phy_position_y += vspd;

/// create dash effect c
var dash = instance_create(x,y,obj_dash_effect);
dash.sprite_index= sprite_index;
dash.image_index = image_index
