///scr_distress 

sprite_index=spr_player_distress;
image_speed = 0.2; 
if image_index > image_number-1{
 
mind_control2=false;
state=states.normal;
}
  

// Check ground
if(place_meeting(x, y+1, obj_solid)  ){
    v_speed = 0;    
    show_debug_message("welp im stuck");
    // Jump
    if(key_jump|| key_up ){
        v_speed = -jumpspeed;
        show_debug_message("welp im stuck while jumping");
        }        
}        

else {
    // Gravity and falling animation
    if(v_speed < 10){
        v_speed += grav; 
      } 
    // Gravity Damping (Keep vertical movement even after jump is released until collide)
    if(key_jump_released && v_speed < -4){
        v_speed = -(jumpspeed/2);    
    }     
}


