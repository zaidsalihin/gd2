///scr_state_normal
/////////////////////////////////////////////////////////////////////////////
 
scr_get_input();
// Get Move Axis
xdir = (key_right - key_left);
ydir = (key_down - key_up);



// Check ground
if(place_meeting(x, y+1, obj_solid)  ){
    v_speed = 0;    
    show_debug_message("welp im stuck");
    // Jump
    if(key_jump|| key_up ){
        v_speed = -jumpspeed;
        show_debug_message("welp im stuck while jumping");
        }        
}        

else {
    // Gravity and falling animation
    if(v_speed < 10){
        v_speed += grav; 
      } 
    // Gravity Damping (Keep vertical movement even after jump is released until collide)
    if(key_jump_released && v_speed < -4){
        v_speed = -(jumpspeed/2);    
    }     
}


// Set h_speed (Check for horizontal damping first)
if(h_speed > movespeed) {
    h_speed = fric * xdir;
} else {
    h_speed = movespeed * xdir;
}


//set mind control 

if (mind_control==true){  
instance_destroy();
instance_create(x,y,obj_rebel);
 
 
 
}

if(run==true)
{
xdir=-1; 
h_speed =3* fric * xdir;
obj_player.xdir=xdir; 
}


if (mind_control2==true){  
xdir=-1;
h_speed =2* fric * xdir;
obj_player.xdir=xdir;
mind_control=false;  
 
}



// Push
scr_push(obj_body);
scr_push(obj_bodycontainer);
scr_push(obj_box_S);
scr_push(obj_container2);
scr_push(obj_body_tied_frozen);
scr_push(obj_body_tied);

// Attack

if(key_attack){
    state = states.attack;
}

// Frozen
if(place_meeting(x, y, obj_ice) || key_freeze){
    state = states.frozen;
} 
scr_state_glow();

// Move
scr_move(h_speed, v_speed);


// Animation 
scr_animate();

 
