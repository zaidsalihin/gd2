///scr_move_state
/////////////////////////////////////////////////////////////////////////////

scr_get_input();

// Exit Game
if(key_exit)
{
    game_end();
}

// Get Move Axis
xdir = (key_right - key_left);
ydir = (key_up - key_down);

///Get Move Horizontal Direction
dir = point_direction(0,0,xdir,ydir);

// Jump
if(key_jump||key_up)
{
    //Limits jump to one.
    if(place_meeting(x,y+5,obj_solid))
    {
        physics_apply_local_impulse(0,0,0,-40);
    }
    //if statement prevents alarm from resetting every time key_jump is pressed.
    if(alarm_get(1) < 0)
    {
        alarm[1] = room_speed/2;
    }
}

// Dash
if(key_dash){
   // state=scr_dash_state;
   // alarm[0] = room_speed/10;
}

// Attack
if(key_attack){
    image_index = 0;
    state = scr_attack_state;
    alarm[0] = room_speed/10
}

// Set hspd
hspd = movespeed * xdir;

// Move character
phy_position_x += hspd;
phy_position_y += vspd;


/// Animation

image_speed = 0.2;

// Idle animation
if (xdir == 0)
{
    //sprite_index = spr_player_idle;
    image_index = 0;
}

if(hspd < 0)
{
sprite_index=spr_player_run;
image_speed = 0.5;
}
else if (hspd >0)
{
sprite_index = spr_player_run;
image_speed = 0.5;
}

// Horizontal animation
// If xdir is moving, set image_xscale to 1 if right and -1 if left.
if (xdir != 0) image_xscale = xdir;
//sprite_index=spr_player_idle;
