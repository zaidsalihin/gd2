///scr_text("Text", animate_speed, x, y, isDraw);

text_created = instance_create(argument2, argument3, obj_text);
text_created.text_creator = id;
with(text_created){

    isDraw = argument4;    

    padding = 8;
    maxlength = 200;
    text = argument0;
    animate_speed = argument1;
    font = coder;
    
    draw_set_font(font);
    text_length = string_length(text);
    font_size = font_get_size(font)
    line_height = font_size + (font_size/2);
    
    text_width = string_width_ext(text, line_height, maxlength);
    text_height = string_height_ext(text, line_height, maxlength);
    
    box_width = text_width + (padding*2);
    box_height = text_height + (padding*2);
    
}
