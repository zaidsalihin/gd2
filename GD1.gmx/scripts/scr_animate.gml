///scr_animate

var objName = object_get_name(object_index);
var sprName = "";
var sprAction = "";
var sprComposed = "";

if (objName == "obj_player"){
    sprName = "spr_player_";
}
else if (objName == "obj_clone"){
    sprName = "spr_clone_";
}

else if (objName == "obj_rebel"){
    sprName = "spr_rebel_";
}

// Idle animation
if (xdir == 0){
    sprAction = "idle";
    sprComposed = sprName + sprAction; 
    sprite_index = asset_get_index(sprComposed);
    image_index = 0;
}
 
// Horizontal animation
if (xdir != 0  && (isJumping==false)){
    sprAction = "run";
    sprComposed = sprName + sprAction; 
    sprite_index = asset_get_index(sprComposed); 
    image_xscale = xdir;
    image_speed = 0.5; 
}

// Vertical animation
if (!place_meeting(x, y, obj_solid)){
    if (yprevious -1 > y){
        sprAction = "jump";
        sprComposed = sprName + sprAction; 
        sprite_index = asset_get_index(sprComposed); 
    }   
    else if (yprevious < y){
        sprAction = "jump_falling";
        sprComposed = sprName + sprAction; 
        sprite_index = asset_get_index(sprComposed); 
    }
}

